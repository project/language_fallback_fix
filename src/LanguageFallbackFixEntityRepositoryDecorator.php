<?php

namespace Drupal\language_fallback_fix;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\TypedData\TranslatableInterface as TranslatableDataInterface;

/**
 * Decorates EntityRepository
 *
 * Implements the core API fix / extension from
 * @see https://www.drupal.org/project/drupal/issues/2951294#comment-13127796
 *
 * How to use:
 * @code
 *   $entityRepository = \Drupal::service('entity.repository');
 *   $entityRepository->getTranslationFromContext($entity, NULL, ['fallback_to_passed_entity' => FALSE])
 * @endcode
 */
class LanguageFallbackFixEntityRepositoryDecorator extends EntityRepositoryDecoratorBase {

  use DependencySerializationTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * @inheritDoc
   */
  public function __construct($decorated, LanguageManagerInterface $languageManager) {
    parent::__construct($decorated);
    $this->languageManager = $languageManager;
  }

  /**
   * Gets the entity translation to be used in the given context.
   *
   * This will check whether a translation for the desired language is available
   * and if not, it will fall back to the most appropriate translation based on
   * the provided context.
   *
   * The semantics of this depends on $context['fallback_to_passed_entity']:
   * - TRUE or unset: @see ::getTranslationFromContextLegacy
   * - FALSE: @see ::getTranslationFromContexAllowNoFallback
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose translation will be returned.
   * @param string $langcode
   *   (optional) The language of the current context. Defaults to the current
   *   content language.
   * @param array $context
   *   (optional) An associative array of arbitrary data that can be useful to
   *   determine the proper fallback sequence.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   An entity object for the translated data.
   *
   * @see \Drupal\Core\Language\LanguageManagerInterface::getFallbackCandidates()
   */
  public function getTranslationFromContext(EntityInterface $entity, $langcode = NULL, $context = []) {
    // As the legacy codepath did not allow a no-fallback scenario, we use this
    // context option to switch between a unchanged legacy version and one that
    // Allows the extended usage.
    // @see https://www.drupal.org/project/drupal/issues/2951294
    // @todo Consider deprecating the legacy codepath.
    $context += ['fallback_to_passed_entity' => TRUE];
    if (!$context['fallback_to_passed_entity']) {
      return $this->getTranslationFromContexAllowNoFallback($entity, $langcode, $context);
    }
    else {
      return $this->getTranslationFromContextLegacy($entity, $langcode, $context);
    }
  }

  /**
   * Gets the entity translation to be used in the given context (allow no
   * fallabck).
   *
   * This will check whether a translation for the desired language is available
   * and if not, it will fall back to the most appropriate translation based on
   * the provided context.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose translation will be returned.
   * @param string $langcode
   *   (optional) The language of the current context. Defaults to the current
   *   content language.
   * @param array $context
   *   (optional) An associative array of arbitrary data that can be useful to
   *   determine the proper fallback sequence.
   *   The following keys have special meanings:
   *   - 'operation': An arbitrary string that will be passed to
   *     hook_language_fallback_candidates_alter. The following options are
   *     defined in core, but more may be defined in contributed modules:
   *       - entity_view: Invoked when an entity is about to be displayed.
   *         The data key contains the loaded entity. (default)
   *         (Note that content_translation removes all fallbacks for
   *         unpublished translations the current user does not have access to.
   *       - entity_upcast: Invoked when an entity parameter is resolved in
   *         routing.
   *       - node_tokens: Used when getting node body/summary tokens.
   *     The following options are defined in core, but in a way that makes no
   *     sense to pass in here.
   *       - views_query: Invoked when a field based views query is performed.
   *         The data key contains a reference to the field object.
   *       - locale_lookup: Invoked when a string translation was not found.
   *         The data key contains the source string.
   *   - 'langcode': Don't use this key, it is used to pass the $langcode to
   *     hook_language_fallback_candidates_alter.
   *   - 'data': Don't use this key, it is used to pass the $entity to
   *     hook_language_fallback_candidates_alter.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   An entity object for the translated data, or null if the given langcode
   *   does not have a translation fallback.
   *
   * @see \Drupal\Core\Language\LanguageManagerInterface::getFallbackCandidates()
   */
  protected function getTranslationFromContexAllowNoFallback(EntityInterface $entity, $langcode, $context) {
    if (!$entity instanceof TranslatableDataInterface ) {
      return $entity;
    }
    $translation = NULL;

    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
        ->getId();
      $entity->addCacheContexts(['languages:' . LanguageInterface::TYPE_CONTENT]);
    }

    // Retrieve language fallback candidates to perform the entity language
    // negotiation, unless the current translation is already the desired one.
    $context['data'] = $entity;
    $context['langcode'] = $langcode;
    $context += ['operation' => 'entity_view'];
    $candidates = $this->languageManager->getFallbackCandidates($context);

    // Ensure the default language has the proper language code.
    $default_language = $entity->getUntranslated()->language();
    if (isset($candidates[$default_language->getId()])) {
      $candidates[$default_language->getId()] = LanguageInterface::LANGCODE_DEFAULT;
    }

    // Return the most fitting entity translation.
    foreach ($candidates as $candidate) {
      if ($entity->hasTranslation($candidate)) {
        $translation = $entity->getTranslation($candidate);
        break;
      }
    }
    return $translation;
  }

  /**
   * Gets the entity translation to be used in the given context (legacy).
   *
   * This will check whether a translation for the desired language is available
   * and if not, it will fall back to the most appropriate translation based on
   * the provided context.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity whose translation will be returned.
   * @param string $langcode
   *   (optional) The language of the current context. Defaults to the current
   *   content language.
   * @param array $context
   *   (optional) An associative array of arbitrary data that can be useful to
   *   determine the proper fallback sequence.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   An entity object for the translated data.
   *
   * @see \Drupal\Core\Language\LanguageManagerInterface::getFallbackCandidates()
   */
  protected function getTranslationFromContextLegacy(EntityInterface $entity, $langcode, $context) {
    $translation = $entity;

    if ($entity instanceof TranslatableDataInterface && count($entity->getTranslationLanguages()) > 1) {
      if (empty($langcode)) {
        $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
          ->getId();
        $entity->addCacheContexts(['languages:' . LanguageInterface::TYPE_CONTENT]);
      }

      // Retrieve language fallback candidates to perform the entity language
      // negotiation, unless the current translation is already the desired one.
      if ($entity->language()->getId() != $langcode) {
        $context['data'] = $entity;
        $context += ['operation' => 'entity_view', 'langcode' => $langcode];
        $candidates = $this->languageManager->getFallbackCandidates($context);

        // Ensure the default language has the proper language code.
        $default_language = $entity->getUntranslated()->language();
        $candidates[$default_language->getId()] = LanguageInterface::LANGCODE_DEFAULT;

        // Return the most fitting entity translation.
        foreach ($candidates as $candidate) {
          if ($entity->hasTranslation($candidate)) {
            $translation = $entity->getTranslation($candidate);
            break;
          }
        }
      }
    }

    return $translation;
  }

}
